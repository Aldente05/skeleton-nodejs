let express = require('express');
let createError = require('http-errors');
let fs = require('fs');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let cors = require('cors')
let bodyParser = require('body-parser');
let {errorHandlers, jwt} = require('./_helpers');

var usersRouter = require('./routes/user-routes');

let app = express();
let corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}
app.use(logger('common', {
    stream: fs.createWriteStream(path.join(__dirname, 'logs/access.log'), {flags: 'a'})
}))
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(cors(corsOptions));

app.use((req, res, next) => {
    req.env = process.env
    next()
})

// uncomment after placing your favicon in /public
app.use(logger('dev'));
// app.use(bodyParser.json());
app.disable('x-powered-by');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(errorHandlers);
app.listen(process.env.LISTEN_PORT);
console.log('Listening on PORT ' + process.env.LISTEN_PORT);
module.exports = app;
