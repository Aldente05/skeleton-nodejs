let User = require('./../models/user-model')
let {utils, role} = require('./../_helpers');
let jwt = require('jsonwebtoken');
let config = require('./../config.json');
let moment = require('moment')
let crypto = require('crypto');

require('moment/locale/id')

// method to check entered password is correct or not
let userService = {
    login: async (email, password) => {
        this.salt = crypto.randomBytes(16).toString('hex');
        let user = await User.findOne({
            where: {
                email: email
            },
            returning: true
        })

        if (user === null) {
            return {code: 400, message: "User not found."}
        } else {
            let hash = crypto.pbkdf2Sync(password, user.salt, 1000, 64, `sha512`).toString(`hex`);
            if (hash === user.hash) {
                delete user.dataValues.salt
                delete user.dataValues.hash
                user.dataValues.token = jwt.sign({
                     uid: user.id,
                     exp: moment().add(7, 'days').valueOf(),
                     scope: role
                 }, config.secret)
                return {code: 200, message: user}
            } else {
                return {code: 403, message: "Wrong Password"}
            }
        }


    },
    register: async (body) => {
        // method to set salt and hash the password for a user
        // setPassword method first creates a salt unique for every user
        // then it hashes the salt with user password and creates a hash
        // this hash is stored in the database as user password
        // creating a unique salt for a particular user
        this.salt = crypto.randomBytes(16).toString('hex');

        // hashing user's salt and password with 1000 iterations,
        // 64 length and sha512 digest
        this.hash = crypto.pbkdf2Sync(body.password, this.salt,
            1000, 64, `sha512`).toString(`hex`);

        return new Promise((resolve, reject) => {
            User.create({
                name: body.name,
                email: body.email,
                gender: body.gender,
                personal_phone_number: body.personal_phone_number,
                salt: this.salt,
                hash: this.hash
            }).then(result => {
                delete result.dataValues.salt
                delete result.dataValues.hash
                resolve(result)
            }).catch(err => reject(err))
        })
    },
    findById: (userId) => {
        return User.findOne({
            where: {
                id: userId
            },
            attributes: {
                exclude: ['salt', 'hash']
            }
        })
    },
    updateUser: (body) => {
        return User.update({
            name: body.name,
            email: body.email,
            gender: body.gender,
            personal_phone_number: body.personal_phone_number,
        }, {
            where: {
                id: body.id
            },
            returning : true
        })
    },
    deleteUser: (body) => {
        return User.destroy({
            where: {
                id: body
            },
            returning : true
        })
    },

    getAll: (body) => {
        return User.findAndCountAll({
            limit: body.limit,
            offset: body.offset,
            attributes: {
                exclude: ['salt', 'hash', 'createdAt', 'updatedAt']
            }
        })
    },
};
module.exports = userService;

