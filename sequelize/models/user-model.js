let sequelize = require('../config/RepositoryConfig');
const Sequelize = require('sequelize');

const user = sequelize.define('user', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    salt: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hash: {
        type: Sequelize.STRING,
        allowNull: false
    },
    personal_phone_number: {
        type: Sequelize.BIGINT,
    },
    gender: {
        type: Sequelize.ENUM,
        values: ["Male", "Female"]
    },
})
module.exports = user;
