/**
 * Created by f.putra on 2019-04-02.
 */
let expressJwt = require('express-jwt');
let jwt = require('jsonwebtoken');
let { secret } = require('./../config.json');
let { utils } = require('./../_helpers');

let authorize = {
    authenticate : (req, res, next) => {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, secret, (err, user) => {
                if (err) {
                    utils.doResponse(res, 403, err);
                }

                req.user = user;
                next();
            });
        } else {
            res.sendStatus(401)
        }
    },
    authorize: (roles = []) => {
        // roles param can be a single role string (e.g. Role.User or 'User')
        // or an array of roles (e.g. [Role.Admin, Role.User] or ['Admin', 'User'])
        if (typeof roles === 'string') {
            roles = [roles];
        }

        return [
            // authenticate JWT token and attach user to request object (req.user)
            expressJwt({ secret }),

            // authorize based on user role
            (req, res, next) => {

                // console.log(roles.includes(req.user.role))
                if (roles.length && !roles.includes(req.user.role)) {
                    // user's role is not authorized
                    return res.status(401).json({ message: 'Unauthorized' });
                }

                // authentication and authorization successful
                next();
            }
        ];
    }
}

module.exports = authorize;
