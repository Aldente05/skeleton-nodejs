const express = require('express');
const userServices = require('./../services/user-services');
const {utils, authorize} = require('./../_helpers');
const router = express.Router();

// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    console.log(req.session)
    if (req.session.user && req.cookies.user_sid) {
    } else {
        next();
    }
};

router.post('/register', (req, res, next) => {
    userServices.register(req.body).then(result => {
        utils.doResponse(res, 200, result);
    }).catch(err => {
        utils.doResponse(res, 400, err);
    })
})

router.post('/login', (req, res, next) => {
    const cret = req.headers['authorization'];
    const decode = new Buffer(cret.split(' ')[1], 'base64').toString();
    const user = decode.split(":")[0];
    const pw = decode.split(":")[1];

    userServices.login(user, pw).then(result => {
        utils.doResponse(res, result.code, result.message);
    }).catch(next)


})
router.patch('/update', (req, res, next) => {
    userServices.updateUser(req.body)
        .then(result => {
            let resultData = result[1][0];
            delete resultData.dataValues.salt
            delete resultData.dataValues.hash
            utils.doResponse(res, 200, resultData);
        }).catch(next)

})

router.get('/find', authorize.authenticate, (req, res, next) => {
    userServices.findById(req.query.id).then(result => {
        utils.doResponse(res, 200, result);
    }).catch(err => {
        utils.doResponse(res, 400, err);
    })

})

router.delete('/delete/:mid', (req, res, next) => {
    userServices.deleteUser(req.params.mid).then(result => {
        utils.doResponse(res, 200, result === 1 ? "DELETE SUCCESS" : "USER NOT FOUND");
    }).catch(next)
})

router.get('/list', (req, res, next) => {
    userServices.getAll({
        offset: parseInt(req.query.offset),
        limit: parseInt(req.query.limit)
    }).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})

module.exports = router;
