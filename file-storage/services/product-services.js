/**
 * Created by f.putra on 07/12/18.
 */
let multer = require('multer');
let Op = require('sequelize').Op
let Product = require('./../models/product-model')
let Media = require('./../models/media-model')
let Stock = require('./../models/stock-model')
let moment = require('moment')

let storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, process.env.STORAGE)
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
let upload = multer({ //multer settings
    storage: storage
}).array('file');

let productServices = {
    create: async (req, res) => {
        return new Promise((resolve, reject) =>{
            upload(req, res, (err) => {
                if (err) {
                    reject({error_code: 1, err_desc: err})
                    return;
                }

                Product.create({
                    productCode: (req.body.name.slice(2) + req.body.brand.slice(2) + req.body.size + Math.floor(Math.random() * 101)).toString().trim(),
                    name: req.body.name,
                    brand: req.body.brand,
                    type: req.body.type,
                    category: req.body.category,
                    description: req.body.description,
                    price: req.body.price,
                    size: req.body.size,
                    sex: req.body.sex,
                    system_inventories: {
                        quantity: req.body.stock,
                        location: "default",
                        stock_in: req.body.stock
                    },
                    media: req.files
                }, {
                    include: [Stock, Media]
                }).then(res => resolve(res)).catch(err => reject(err))
            });
        })
    },

    update: (body) => {
        return Product.update({
            brand: body.brand,
            type: body.type,
            category: body.category,
            description: body.description,
            size: body.size,
            sex: body.sex,
            stock: body.stock
        }, {
            where: {
                id: body.id
            }
        })
    },

    findById: (idProduct) => {
        return Product.findByPk(idProduct, {
            include: [{
                model: Media,
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'id']
                },
            }],
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
        })
    },

    findByProductCode: (productCode) => {
        return Product.findOne({
            where: {
                productCode: productCode
            },
            include: [{
                model: Media,
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'id']
                },
            }],
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
        })
    },

    delete: (body) => {
        return Product.update(body)
    },

    getAll: (body) => {
        return Product.findAndCountAll({
            limit: body.limit,
            offset: body.offset,
            include: [{
                model: Media,
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'id']
                },
            }, {
                model: Stock,
            }],
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
        })
    },

    topGrossing: (offset, limit) => {
        return Product.findAndCountAll({
            offset: offset,
            limit: limit,
            include: [{
                model: Media,
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'id']
                },
            }],
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
        })
    },

    newComming: (offset, limit) => {
        return Product.findAndCountAll({
            where: {
                createdAt: {
                    [Op.lt]: new moment().endOf('day').toDate(),
                    [Op.gt]: new moment().startOf('day').toDate()
                }
            },
            offset: offset,
            limit: limit,
            include: [{
                model: Media,
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'id']
                },
            }],
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
        })
    },
};
module.exports = productServices;
