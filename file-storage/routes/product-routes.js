/**
 * Created by f.putra on 07/12/18.
 */
let express = require('express');
let router = express.Router();
let productServices = require('../services/product-services');
let {utils} = require('./../_helpers');

router.post('/create', (req, res, next) => {
    productServices.create(req, res).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})

router.get('/code=:mid', (req, res, next) => {
    productServices.findByProductCode(req.params.mid).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})
router.get('/top/offset=:moffset&limit=:mlimit', (req, res, next) => {
    productServices.topGrossing(req.params.moffset, req.params.mlimit).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})
router.get('/new/offset=:moffset&limit=:mlimit', (req, res, next) => {
    productServices.newComming(req.params.moffset, req.params.mlimit).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})

router.patch('/update', (req, res, next) => {
    productServices.update(req.body).then(result => {
        utils.doResponse(res, 200, result[1][0])
    }).catch(next)
})
router.delete('/delete/:mid', (req, res, next) => {
    productServices.delete(req.params.mid).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})

router.get('/list', (req, res, next) => {
    productServices.getAll({
        offset : parseInt(req.query.offset),
        limit : parseInt(req.query.limit)
    }).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})

router.get('/:mid', (req, res, next) => {
    productServices.findById(req.params.mid).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(next)
})

module.exports = router;
