/**
 * Created by f.putra on 07/12/18.
 */
let sequelize = require('../config/RepositoryConfig');
let Product = require('../models/product-model');
let Sequelize = require('sequelize');

const Inventory = sequelize.define('system_inventory', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    productId : {
        type: Sequelize.BIGINT
    },
    quantity: {
        type: Sequelize.STRING
    },
    location: {
        type: Sequelize.STRING
    },
    stock_in: {
        type: Sequelize.BIGINT
    },
    stock_out: {
        type: Sequelize.BIGINT
    }
});
// Inventory.belongsToM(Product)
module.exports = Inventory;
