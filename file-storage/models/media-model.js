/**
 * Created by f.putra on 15/01/19.
 */
let sequelize = require('../config/RepositoryConfig');
let Sequelize = require('sequelize');

const mediaSchema = sequelize.define('media', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    path: {
        type: Sequelize.STRING
    },
    productId : {
        type: Sequelize.BIGINT
    }
});


module.exports = mediaSchema;
