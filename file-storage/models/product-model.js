/**
 * Created by f.putra on 07/12/18.
 */
let sequelize = require('../config/RepositoryConfig');
let Sequelize = require('sequelize');
let Media = require('./media-model')
let Stock = require('./stock-model')

const Product = sequelize.define('product', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    productCode : {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
    brand: {
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.STRING
    },
    category: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.BIGINT
    },
    size: {
        type: Sequelize.STRING
    },
    sex: {
        type: Sequelize.ENUM,
        values: ['Female', 'Male']
    }
});

Product.Media = Product.hasMany(Media)
Product.Stock = Product.hasMany(Stock)

module.exports = Product;


