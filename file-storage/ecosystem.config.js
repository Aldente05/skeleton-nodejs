const {Op} = require('sequelize');

module.exports = {
    apps: [{
        name: 'FILE-STORAGE SERVICE EXAMPLE',
        script: 'file-storage-server.js',
        instances : 1,
        exec_mode : "fork",
        watch: true,
        error_file: "./logs/err.log",
        out_file: "./logs/out.log",
        merge_logs: true,
        log_date_format: "YYYY-MM-DD HH:mm Z",
        ignore_watch: [".idea", "node_modules", ".git", "uploads", "logs"],
        env_development: {
            LISTEN_PORT : 3000,
            STORAGE : "uploads",
            NODE_ENV: 'development',
            SESSION: JSON.stringify({
                name: 'session',
                keys: ['key1', 'key2'],
                cookie: {
                    secure: true,
                    httpOnly: true,
                    domain: 'example.com',
                    path: 'foo/bar',
                    expires: new Date(Date.now() + 60 * 60 * 1000)
                }
            }),
            CONFIG: JSON.stringify({
                username: 'root',
                password: 'root',
                database: 'skeleton',
                host: 'localhost',
                port: '5432',
                dialect: 'postgres',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            })
        },
        env_staging: {
            LISTEN_PORT : 3500,
            STORAGE : "uploads",
            NODE_ENV: 'staging',
            CONFIG: JSON.stringify({
                username: 'root',
                password: 'root',
                database: 'root',
                host: '127.0.0.1',
                port: '5432',
                dialect: 'postgres',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            }),
            SESSION: JSON.stringify({
                name: 'session',
                keys: ['key1', 'key2'],
                cookie: {
                    secure: true,
                    httpOnly: true,
                    domain: 'example.com',
                    path: 'foo/bar',
                    expires: new Date(Date.now() + 60 * 60 * 1000)
                }
            })
        },
        env_production: {
            LISTEN_PORT : 3500,
            STORAGE : "uploads",
            NODE_ENV: 'production',
            SESSION: JSON.stringify({
                name: 'session',
                keys: ['key1', 'key2'],
                cookie: {
                    secure: true,
                    httpOnly: true,
                    domain: 'example.com',
                    path: 'foo/bar',
                    expires: new Date(Date.now() + 60 * 60 * 1000)
                }
            }),
            CONFIG: JSON.stringify({
                username: 'root',
                password: 'root',
                database: 'root',
                host: '127.0.0.1',
                port: '5432',
                dialect: 'mysql',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            }),
        }
    }]
};
